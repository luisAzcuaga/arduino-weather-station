#ifndef LCDMODE_H
#define LCDMODE_H

// SCREEN LIBRARY
#include <LiquidCrystal_I2C.h>

class LcdMode
{
public:
  LcdMode();
  void begin();
  void refresh();
};

extern LcdMode lcdmode;

#endif // LCDMODE_H