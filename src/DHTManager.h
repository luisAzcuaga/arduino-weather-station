#ifndef DHTMANAGER_H
#define DHTMANAGER_H
#include "libraries.h"

class DHTManager
{
public:
  DHTManager();
  void begin();
  float readHumidity();
  float readTemperature();
  float computeHeatIndex(float, float, bool);
};

extern DHTManager dhtmanager;

#endif //DHTMANAGER_H