#include "DHTManager.h"

static DHTManager *self;
DHT dht(5, DHT22);

DHTManager::DHTManager()
{
  self = this;
}

void DHTManager::begin()
{
  dht.begin();
}
float DHTManager::readHumidity()
{
  return dht.readHumidity();
}
float DHTManager::readTemperature()
{
  return dht.readTemperature();
}

float DHTManager::computeHeatIndex(float temperature, float humidity, bool fahrenheit)
{
  return dht.computeHeatIndex(temperature, humidity, false);
}
