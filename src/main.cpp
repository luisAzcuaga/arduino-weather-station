#include "config.h"
unsigned long read_timer = 0;

#ifdef LED_BLINK
  unsigned long led_timer = 0;
#endif

#ifdef OLED
#include "OledMode.h"
OledMode oledmode;
#endif
#ifdef LCD
#include "LcdMode.h"
LcdMode lcdmode;
#endif

void setup()
{
  #ifdef OLED
  oledmode.begin();
  #endif
  #ifdef LCD
  lcdmode.begin();
  #endif
  #ifdef LED_BLINK
    pinMode(LED_PIN,OUTPUT);
  #endif
}

void loop()
{
  unsigned long currentMillis = millis();
  if (currentMillis >= read_timer + READ_WAIT)
  {
    read_timer = currentMillis;
    #ifdef LED_BLINK
    digitalWrite(LED_PIN, HIGH);
    #endif

    #ifdef OLED
    oledmode.refresh();
    #endif

    #ifdef LCD
    lcdmode.refresh();
    #endif
  }
  #ifdef LED_BLINK
  if (currentMillis >= led_timer + 200)
  {
    led_timer = currentMillis;
    digitalWrite(LED_PIN, LOW);
  };
  #endif
}