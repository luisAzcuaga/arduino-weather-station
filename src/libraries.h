// CORE LIBRARIES
#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>

// SENSOR LIBRARIES
#include <Adafruit_BMP085.h>
#include <DHT.h>