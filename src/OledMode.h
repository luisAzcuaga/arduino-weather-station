#ifndef OLEDMODE_H
#define OLEDMODE_H

// SCREEN LIBRARIES
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1    // Reset pin # (or -1 if sharing Arduino reset pin)

class OledMode
{
public:
  OledMode(); // constructor
  void begin();
  void refresh();
};

extern OledMode oledmode;

#endif // OLEDMODE_H
