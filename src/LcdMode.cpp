#include "config.h"
#include "LcdMode.h"
#include "BMPManager.h"
#include "DHTManager.h"

// Icons for LCD screen
#include "StationIcons.h"

static LcdMode *self;
LiquidCrystal_I2C lcd(0x3F, 16, 2);
#ifdef LCD
DHTManager dhtmanager;
BMPManager bmpmanager;
#endif

LcdMode::LcdMode()
{
  self = this;
}

void LcdMode::begin()
{
  dhtmanager.begin();
  bmpmanager.begin();

  lcd.begin();
  lcd.backlight();
  lcd.createChar(0, temp);
  lcd.createChar(1, celsius);
  lcd.createChar(2, tempSensation);
  lcd.createChar(3, pressure);
  lcd.createChar(4, humidity);
}

void LcdMode::refresh()
{

  float temp, pressure, humidity, tempDHT, heatIndex;

  temp = bmpmanager.readTemperature();
  pressure = bmpmanager.readTemperature();
  humidity = dhtmanager.readHumidity();
  //tempDHT = dhtmanager.readTemperature(); // Useless unless you don't have any other way to measure temperature
  heatIndex = dhtmanager.computeHeatIndex(temp, humidity, false);

  lcd.clear();
  lcd.home();

  // TEMPERATURE
  lcd.write(byte(0));
  lcd.print(temp, 1);
  lcd.write(byte(1));
  lcd.print(" ");

  // HEAT INDEX
  lcd.write(byte(2));
  lcd.print(heatIndex, 1);
  lcd.write(byte(1));

  lcd.setCursor(0, 1);

  // HUMIDITY
  lcd.write(byte(4));
  lcd.print(humidity);
  lcd.print("%");
  lcd.print(" ");

  // PRESSURE
  lcd.write(byte(3));
  lcd.print(pressure, 0);
  lcd.print("Pa");
}
