#include "BMPManager.h"

static BMPManager *self;
Adafruit_BMP085 bmp180;

BMPManager::BMPManager()
{
  self = this;
}

void BMPManager::begin()
{
  bmp180.begin();
}
float BMPManager::readTemperature()
{
  return bmp180.readTemperature();
}

float BMPManager::readPressure()
{
  return bmp180.readPressure() / 100;
}