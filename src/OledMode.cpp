#include "config.h"
#include "OledMode.h"
#include "BMPManager.h"
#include "DHTManager.h"

static OledMode *self;
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#ifdef OLED
DHTManager dhtmanager;
BMPManager bmpmanager;
#endif

OledMode::OledMode()
{
  self = this;
}

void OledMode::begin()
{
  dhtmanager.begin();
  bmpmanager.begin();
  Serial.begin(9600);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }

  display.clearDisplay();
  display.setTextColor(WHITE);
  display.println("Estacion\nmeteorologica! v1.2.1\nHecha por");
  display.println("\nLuis\nAzcuaga");
  display.setTextSize(2);
  display.display();
  delay(2000);
}

void OledMode::refresh()
{
  float temp, pressure, humidity, tempDHT, heatIndex;

  temp = bmpmanager.readTemperature();
  pressure = bmpmanager.readPressure();
  humidity = dhtmanager.readHumidity();
  // tempDHT = dhtmanager.readTemperature(); // Useless unless you don't have any other way to measure temperature
  heatIndex = dhtmanager.computeHeatIndex(temp, humidity, false);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.display();
  
  // TEMPERATURE
  display.write(15);
  display.print(temp, 1);
  display.write(248);
  display.println("c");

  // HEAT INDEX
  display.write(15);
  display.print(heatIndex, 1);
  display.write(248);
  display.println("c");

  // HUMIDITY
  display.print(humidity, 1);
  display.println("%");

  // PRESSURE
  display.print(pressure, 0);
  display.print(" ");
  display.println("hPa");

  display.display();
}