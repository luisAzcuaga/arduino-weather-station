#ifndef BMPMANAGER_H
#define BMPMANAGER_H
#include "libraries.h"

class BMPManager
{
public:
  BMPManager();
  void begin();
  float readTemperature();
  float readPressure();
};

extern BMPManager bmpmanager;

#endif //BMPMANAGER_H