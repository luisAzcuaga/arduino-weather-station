# Basic Arduino Weather Station

#### Tested with this hardware:
- Board:
  - Arduino UNO
  - NodeMCU (ESP32 DO IT DEVKIT v1)
- Screen:
  - LCD 16x2 screen (using the I2C conversor)
  - OLED 128x64 (altho mine is SPI/I2C I used I2C configuration)
- Sensors:
  - BMP180
  - DHT22

## Configuration
In case you clone this repo there are a couple of stuff you should change to get it going
- Look up for the platformio.ini file (_./platformio.ini_)
  - Since I built and compiled this project using platformio, here you can choose which board the project will be compiled on; simply comment the board you'll be using (arduino or esp32 are the ones I tried my code on)
```ini
[env:uno]
platform = atmelavr
board = uno
framework = arduino
; [env:esp32doit-devkit-v1]
; platform = espressif32
; board = esp32doit-devkit-v1
; framework = arduino
```
For example, this would be the configuration to stat off with an Arduino UNO.

- There is a config.h file (_./src/config.h_)
  - Choose whether you are going to use an __OLED DISPLAY__ or __LCD DISPLAY__
  - Choose the reading interval, there's no case in lowering the number below 1000 miliseconds since the sensors (specially the DHT22) need some time to get enough data to send; otherwise you might get some noise readings.

By default, this project is configured to work with an ESP32 and a OLED screen with 5 seconds between readings.

## Setup 
The connections are up to you ;)

It's I2C anyway, so OLED/LCD screen and BMP180 share same SCL and SDA pins. The DHT22 will use Digital pin 5 (It's the one I used, there is nothing special with the pin)
As for the I2C pins, simply look for the SCL and SDA pins on your board:
#### Arduino UNO:
- SDA -> A4
- SCL -> A5

Keep in mind Arduino UNO has an extension for SDA and SCL right after all the digital pins, near the USB port (use the ones suit you best)

#### ESP32:
- SDA -> D21
- SCL -> D22

### I2C conversor
Originally my LCD was without this conversor, so I had to use tons of wires to get it working, bought this cheap conversor to ease wiring (the library needed is included in this project).

**Note:** If you are not using an Arduino UNO you might need to change the the memory value used on the LiquidCrystal_I2C instanciation call.

### BMP180
This is a very cheap sensor and will provide us with both _temperature and atmosferic pressure_ (and it's pretty accurate!).

### DHT22
Compared to his little brother DHT11, DHT22 is not so cheap, altho the main benefit its the humidity range goes from 0% to 100%, rather 20% to 80% of the DHT11; As you see, in my city that 80% is easily exceeded.
This one will provide us with _temperature and humidity_ (yep, temperature too), it's library will gives us (besides temp and humidity in both °F and °C) also heat index (which is the relation between humidity and temperature)

## Next Updates
- [TODO] Organize better the project if possible
- [TODO] Consume wifi capabilities of ESP32
- [TODO] Create icons for OLED screen